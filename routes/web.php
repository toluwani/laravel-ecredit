<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () { // return view('welcome'); // });



Route::get('/', function () {
    return view('home');
})->name('home');




    Route::get('/rt', function () {
        return view('dash.history');
    });




//signup routes
Route:: get('/r','authcontroller@getsignup');
Route:: post('/r','authcontroller@postsignup');


//signin routes

Route:: get('/l','authcontroller@getsigin');
Route:: post('/l','authcontroller@postsigin');

// logout route
Route::get('/logout','authcontroller@destroy')->middleware('auth');


// buy now controller 

Route:: get('/buy','paymentcontroller@getbuy');
Route:: post('/b','paymentcontroller@store');

// dashboard routes

Route::get('/dash', 'dashcontroller@index' )->name('dash')->middleware('auth');
Route::get('/u', 'dashcontroller@getuser' )->name('dash.user')->middleware('auth');
Route::get('/h', 'dashcontroller@gethistory' )->name('dash.history')->middleware('auth');



    //admin routes


    Route::get('/admin', 'admincontroller@getadmin' )->name('dash.admin')->middleware('auth');
    Route:: get('/update/{id}','admincontroller@update');
    Route:: post('/edit/{id}','admincontroller@edit');
