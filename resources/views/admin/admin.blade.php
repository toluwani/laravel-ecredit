@extends('dash.master') 

@section ('content')




<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">  Transaction history  </h4>
                        <p class="card-category"> Transaction history of users</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>id  </th>
                                    <th>Date</th>
                                    <th> Payment Type </th>
                                    <th> Network</th>
                                    <th>Airtime amount </th>
                                    <th>phone no</th>
                                    <th> edit e-wallet</th>
                                </thead>
                                <tbody>

                                       
                                        @foreach ($payment as $p)
                                    <tr>

                                        <td>{{$p->id}} </td>
                                        <td>{{$p-> created_at-> format('M d, Y ') }} </td>
                                        <td> {{$p->payup }}</td>
                                        <td>{{$p->network}}</td>
                                        <td>{{$p->airtime}}</td>
                                        <td>{{$p->phone }}</td>
                                        <td>   <a href="#" class="btn btn-info" role="button"> edit </a></td>

                                        
                                        
                                                   </tr>

                                                   @endforeach

                                             
                                                                  </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>




@endsection