@extends('dash.master') 

@section ('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">  Buy Data bundle  </h4>
                        <p class="card-category"> cheap internet data bundle</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>id  </th>
                                    <th>datbundle </th>
                                    <th> MTN </th>
                                    <th>Airtel </th>
                                    <th>Glo</th>
                                    <th>Etisalat </th>
                                    <th> Buy Now </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1 </td>
                                        <td>Dakota Rice </td>
                                        <td>Niger </td>
                                        <td>Oud-Turnhout </td>
                                        <td>Niger </td>
                                        <td>Niger</td>

                                        <td class="text-primary">
                                 <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2 </td>
                                        <td>Minerva Hooper </td>
                                        <td>Curaçao </td>
                                        <td>Sinaai-Waas </td>
                                        <td>Niger </td>
                                        <td>Niger</td>
                                        <td class="text-primary">
                                                <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                       
                                                       </td>
                                    </tr>
                                    <tr>
                                        <td>3 </td>
                                        <td>Sage Rodriguez </td>
                                        <td>Netherlands </td>
                                        <td>Baileux </td>
                                        <td>Niger </td>
                                        <td>Niger</td>

                                        <td class="text-primary">
                                                <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                       
                                                       </td>

                                    </tr>
                                    <tr>
                                        <td>4 </td>
                                        <td>Philip Chaney </td>
                                        <td>Korea, South </td>
                                        <td>Overland Park </td>
                                        <td>Niger </td>
                                        <td>Niger</td>
                                        <td class="text-primary">
                                                <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                       
                                                       </td>                                    </tr>
                                    <tr>
                                        <td>5 </td>
                                        <td>Doris Greene </td>
                                        <td>Malawi </td>
                                        <td>Feldkirchen in Kärnten </td>
                                        <td>Niger </td>
                                        <td>Niger</td>
                                        <td class="text-primary">
                                                <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                       
                                                       </td>                                    </tr>
                                    <tr>
                                        <td>6 </td>
                                        <td>Mason Porter </td>
                                        <td>Chile </td>
                                        <td>Gloucester </td>
                                        <td>Niger </td>
                                        <td>Niger</td>
                                        <td class="text-primary">
                                                <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                       
                                                       </td>                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title mt-0"> Buy airtime</h4>
                        <p class="card-category">buy cheap internet data bundle</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                                <table class="table">
                                        <thead class=" text-primary">
                                            <th>id  </th>
                                            <th>datbundle </th>
                                            <th> MTN </th>
                                            <th>Airtel </th>
                                            <th>Glo</th>
                                            <th>Etisalat </th>
                                            <th> Buy Now </th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1 </td>
                                                <td>Dakota Rice </td>
                                                <td>Niger </td>
                                                <td>Oud-Turnhout </td>
                                                <td>Niger </td>
                                                <td>Niger</td>
        
                                                <td class="text-primary">
                                                        <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                               
                                                               </td>
                                            </tr>
                                            <tr>
                                                <td>2 </td>
                                                <td>Minerva Hooper </td>
                                                <td>Curaçao </td>
                                                <td>Sinaai-Waas </td>
                                                <td>Niger </td>
                                                <td>Niger</td>
                                                <td class="text-primary">
                                                        <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                               
                                                               </td>                                            </tr>
                                            <tr>
                                                <td>3 </td>
                                                <td>Sage Rodriguez </td>
                                                <td>Netherlands </td>
                                                <td>Baileux </td>
                                                <td>Niger </td>
                                                <td>Niger</td>
                                                <td class="text-primary">
                                                        <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                               
                                                               </td>                                            </tr>
                                            <tr>
                                                <td>4 </td>
                                                <td>Philip Chaney </td>
                                                <td>Korea, South </td>
                                                <td>Overland Park </td>
                                                <td>Niger </td>
                                                <td>Niger</td>
                                                <td class="text-primary">
                                                        <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                               
                                                               </td>                                            </tr>
                                            <tr>
                                                <td>5 </td>
                                                <td>Doris Greene </td>
                                                <td>Malawi </td>
                                                <td>Feldkirchen in Kärnten </td>
                                                <td>Niger </td>
                                                <td>Niger</td>
                                                <td class="text-primary">
                                                        <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                               
                                                               </td>                                            </tr>
                                            <tr>
                                                <td>6 </td>
                                                <td>Mason Porter </td>
                                                <td>Chile </td>
                                                <td>Gloucester </td>
                                                <td>Niger </td>
                                                <td>Niger</td>

                                                <td class="text-primary">
                                                        <a href="{{'/buy'}}" class="btn btn-info" role="button">Buy now</a>
                                                               
                                                               </td>                                                
                                            </tr>
                                        </tbody>
                                    </table>                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection