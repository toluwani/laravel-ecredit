<div class="sidebar-wrapper">
    <ul class="nav"> 
        <li class="nav-item active ">
            <a class="nav-link" href="{{'/u'}}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href=" {{'/h'}}">
                <i class="material-icons">history</i>
                <p> History</p>
            </a>
            
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{'/buy'}}">
                <i class="material-icons"> send</i>
                <p>Buy now</p>
            </a>
        </li>



        <li class="nav-item ">
                <a class="nav-link" href="{{'/'}}">
                    <i class="material-icons">account_balance</i>
                    <p> Home page </p>
                </a>
            </li>


            <li class="nav-item ">
                    <a class="nav-link" href="{{'/logout'}}">
                        <i class="material-icons">cancel</i>
                        <p> logout</p>
                    </a>
                </li>
        
    </ul>
</div>