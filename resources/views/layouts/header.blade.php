<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title> e-credit </title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="/style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="/css/responsive.css" rel="stylesheet">

</head>

<body>
    <!-- Preloader Start -->
    {{--  <div id="preloader">
        <div class="colorlib-load"></div>
    </div>  --}}

    <!-- ***** Header Area Start ***** -->
    <header class="header_area animated">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-10">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="#"> e-credit</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                         @if (Auth::check())  

                                    <li class="nav-item active"><a class="nav-link" href="#home">Home</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{"/buy"}}">BuyNow</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#pricing">Pricing</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#testimonials">Testimonials</a></li>
                                    {{-- <li class="nav-item"><a class="nav-link" href="{{"/l"}}">login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{"/r"}}">Register</a></li> --}}
                                    <li class="nav-item"><a class="nav-link" href="{{"/logout"}}">logout</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{"/u"}}">Dashboard</a></li>

                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>

                                    @else
   
                                    <li class="nav-item active"><a class="nav-link" href="#home">Home</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                                   
                                    <li class="nav-item"><a class="nav-link" href="{{"/buy"}}">BuyNow</a></li>
                                  
                                    <li class="nav-item"><a class="nav-link" href="#pricing">Pricing</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#testimonials">Testimonials</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{"/l"}}">login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{"/r"}}">Register</a></li>
                                
                                  
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>

                                    @endif


                                </ul>
                                {{--  <div class="sing-up-button d-lg-none">
                                    <a href="#">Sign Up Free</a>
                                </div>  --}}
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->

                
{{--  
                <div class="col-11col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="{{"/l"}}">Sign Up Free</a>
                    </div>
                </div>
                  --}}
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
