<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title> e-credit </title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="/style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="/css/responsive.css" rel="stylesheet">

</head>

<body>




        <header class="header_area animated " >

<section class="header_area animated sticky slideIndown clearfix " id="">
        <div class="container-fluid">
                <div class="row align-items-center">
                    <!-- Menu Area Start -->
                    <div class="col-12 col-lg-10">
                        <div class="menu_area">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <!-- Logo -->
                                <a class="navbar-brand" href="#"> e-credit</a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                                <!-- Menu Area -->
                                <div class="collapse navbar-collapse" id="ca-navbar">
                                    <ul class="navbar-nav ml-auto" id="nav">
                                        @if (Auth::check())  

                                        <li class="nav-item "><a class="nav-link" href="{{"/"}}#home">Home</a></li>
                                        <li class="nav-item"><a class="nav-link" href="{{"/"}}#about">About</a></li>
                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#features">Features</a></li>
                                        <li class="nav-item"><a class="nav-link" href="{{"/buy"}}">Buy now</a></li>
                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#pricing">Pricing</a></li>
                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#testimonials">Testimonials</a></li>

                                        <li class="nav-item"><a class="nav-link" href="{{"/u"}}">Dashboard</a></li>

                                            <li class="nav-item"><a class="nav-link" href=" {{"/logout"}}">logout</a></li>

                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#contact">Contact</a></li>

                                        @else




                                        <li class="nav-item "><a class="nav-link" href="{{"/"}}#home">Home</a></li>
                                        <li class="nav-item"><a class="nav-link" href="{{"/"}}#about">About</a></li>
                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#features">Features</a></li>
                                            {{-- <li class="nav-item"><a class="nav-link" href="{{"/buy"}}">Buy now</a></li> --}}
                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#pricing">Pricing</a></li>
                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#testimonials">Testimonials</a></li>

                                         <li class="nav-item"><a class="nav-link" href="{{"/l"}}">login</a></li>
                                            <li class="nav-item"><a class="nav-link" href="{{"/r"}}">Register</a></li> 

                                        <li class="nav-item"><a class="nav-link" href=" {{"/"}}#contact">Contact</a></li>
                                           @endif

                                    </ul>
                                    {{--  <div class="sing-up-button d-lg-none">
                                        <a href="{{"/r"}}">Sign Up Free</a>
                                    </div>  --}}
                                </div>
                            </nav>
                        </div>
                    </div>
                    <!-- Signup btn -->
                    {{--  <div class="col-12 col-lg-2">
                        <div class="sing-up-button d-none d-lg-block">
                            <a href="#">Sign Up Free</a>
                        </div>
                    </div>  --}}
                </div>
            </div>
    



   
        
        
        
        
   
</section>

<header>








